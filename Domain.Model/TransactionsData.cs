﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS.LogKata.Domain.Model
{
	public struct TransactionsData
	{
		public decimal Balance { get; set; }

		public decimal Total_Fees { get; set; }

		public decimal Tranferred_To_Recipient { get; set; }

	}
}
