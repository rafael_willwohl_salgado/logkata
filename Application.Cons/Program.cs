﻿using RWS.LogKata.Domain.Core;
using RWS.LogKata.Domain.Service;
using System;

namespace RWS.LogKata.Application.Cons
{
	class Program
	{
		static void Main(string[] args)
		{
			ITransactionsService transactionsService = new TransactionsService();
			do
			{				
				Console.WriteLine("Type your transactions and press ENTER to see the report:");
				string transactionsRaw = Console.ReadLine().Replace("\\n", "\n");
				Console.WriteLine();							
				Console.Write(transactionsService.Report(transactionsRaw));
				Console.WriteLine();
				Console.WriteLine("Press N to shutdown or any other key to continue...");
				Console.WriteLine();
				Console.WriteLine();

			} while (Console.ReadKey().Key != ConsoleKey.N);
		}		
	}
}
