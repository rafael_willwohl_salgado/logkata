﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RWS.LogKata.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RWS.LogKata.Application.Api.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class ReportController : ControllerBase
	{

		private readonly ILogger<ReportController> logger;
		private readonly ITransactionsService transactionService;

		public ReportController(ILogger<ReportController> logger, ITransactionsService transactionService)
		{
			this.logger = logger;
			this.transactionService = transactionService;
		}

		[HttpPost]
		public IActionResult Post([FromBody] string transactionsRaw)
		{
			return Ok(transactionService.Report(transactionsRaw));
		}
	}
}
