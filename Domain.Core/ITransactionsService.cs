﻿using RWS.LogKata.Domain.Model;

namespace RWS.LogKata.Domain.Core
{
	public interface ITransactionsService
	{
		public string Report(string transationsData);
	}
}
