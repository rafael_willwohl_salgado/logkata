﻿using RWS.LogKata.Domain.Core;
using RWS.LogKata.Domain.Model;
using System;

namespace RWS.LogKata.Domain.Service
{
	public class TransactionsService : ITransactionsService
	{
		public string Report(string transactionsData)
		{
			var data = TransactionsCalculator(transactionsData);
			return ResultReport(data);
		}

		private TransactionsData TransactionsCalculator(string transactionsData)
		{
			var transactionResult = new TransactionsData();

			var transactionsRaw = transactionsData.Split('\n', StringSplitOptions.RemoveEmptyEntries);
		
	        foreach(var transactionRaw in transactionsRaw)
			{
				switch (Enum.Parse(typeof(TransactionTypeEnum), transactionRaw.ToUpperInvariant().Substring(0, 1)))
				{
					case TransactionTypeEnum.P:
						IncreaseBalance(decimal.Parse(transactionRaw[1..]), ref transactionResult);
						break;
					case TransactionTypeEnum.R:
						DecreaseBalance(decimal.Parse(transactionRaw[1..]), ref transactionResult);
						break;
					case TransactionTypeEnum.F:
						ApplyFee(decimal.Parse(transactionRaw[1..]), ref transactionResult);
						break;
					case TransactionTypeEnum.L:
						Payout(ref transactionResult);
						break;
				}
			}
			return transactionResult;
		}

		private static void IncreaseBalance(decimal value, ref TransactionsData data)
		{

			data.Balance += value;
		}

		private static void DecreaseBalance(decimal value, ref TransactionsData data)
		{

			data.Balance -= value;
		}

		private static void ApplyFee(decimal value, ref TransactionsData data)
		{

			DecreaseBalance(value, ref data);
			data.Total_Fees += value;
		}

		private static void Payout(ref TransactionsData data)
		{
			/*if (result.Balance > 0)
			{*/
			data.Tranferred_To_Recipient += data.Balance;
			data.Balance = decimal.Zero;
			//}
		}

		private static string ResultReport(TransactionsData data)
		{

			return $"Balance: {data.Balance} euros\n" +
				   $"Total fees: {data.Total_Fees} euros\n" +
				   $"Transferred to recipient: {data.Tranferred_To_Recipient} euros";
		}
	}
}
