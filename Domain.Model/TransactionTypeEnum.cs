﻿namespace RWS.LogKata.Domain.Model
{
	public enum TransactionTypeEnum
	{
		/// <summary>
		/// Payment: Add the amount to the balance
		/// </summary>
		P,
		/// <summary>
		/// Refund: Withdraws the amount from the balance
		/// </summary>
		R,
		/// <summary>
		/// Refund: Withdraws the amount from the balance and adds it to the total fees
		/// </summary>
		F,
		/// <summary>
		/// Payout: Reset balance to zero (it is transferred to the recipient)
		/// </summary>
		L
	}
}
